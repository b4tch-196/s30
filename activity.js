db.fruits.aggregate([

	{$match: {$and:[{supplier:"Yellow Farms"},{price:{$lt:50}},{onSale:true}]}},
	{$count: "countYellowFarmsItemLowerThan50"}

])

db.fruits.aggregate([

	{$match: {price:{$lt:30}}},
	{$count: "itemsPriceLessThan30"}

])

db.fruits.aggregate([

	{$match: {$and:[{supplier:"Yellow Farms"},{onSale:true}]}},
	{$group: {_id:"$supplier", avgPrice:{$avg:"$price"}}}

])

db.fruits.aggregate([

	{$match: {$and:[{supplier:"Red Farms Inc."},{onSale:true}]}},
	{$group: {_id:"$supplier", highestPrice:{$max:"$price"}}}

])

db.fruits.aggregate([

	{$match: {$and:[{supplier:"Red Farms Inc."},{onSale:true}]}},
	{$group: {_id:"$supplier", highestPrice:{$min:"$price"}}}

])

